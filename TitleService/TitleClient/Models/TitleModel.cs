using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TitleService.Models
{
    public partial class TitleModel
    {
        [DisplayAttribute(Name = "Title Id")]
        public int TitleId { get; set; }

        [DisplayAttribute(Name = "Title Name")]
        public string TitleName { get; set; }

        [DisplayAttribute(Name = "Sortable")]
        public string TitleNameSortable { get; set; }

        [DisplayAttribute(Name = "Title Type Id")]
        [DisplayFormat(NullDisplayText="Title TypeId not specified")]
        public int?TitleTypeId { get; set; }

        [DisplayAttribute(Name = "Release Year")]
        public int?ReleaseYear { get; set; }

        [DisplayAttribute(Name = "Processed DateTime")]
        public DateTime?ProcessedDateTimeUTC { get; set; }

    }
}
