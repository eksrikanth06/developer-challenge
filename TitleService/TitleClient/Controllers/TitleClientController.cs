﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;
using TitleService.Models;
using PagedList;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;

namespace TitleClient.Controllers
{
    public class TitleClientController : Controller
    {
        //
        // GET: /TitleClient/
        private static IEnumerable<TitleModel> titleModel;
        [OutputCache(Duration=20)]
        public async Task<ActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "TitleName" : "";
            ViewBag.DateSortParm = sortOrder == "ReleaseYear" ? "ReleaseYear)" : "ReleaseYear";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            

            titleModel = new List<TitleModel>();
            HttpClient client =  new HttpClient();
            client.BaseAddress = new Uri(@"http://localhost:4374/");

            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync("api/title");
            if (response.IsSuccessStatusCode)
            {
                titleModel =  response.Content.ReadAsAsync<List<TitleModel>>().Result;
            }

            if (!string.IsNullOrWhiteSpace(Request.QueryString["TitleName"]))
            {
                searchString=Request.QueryString["TitleName"].ToString();
            }

            List<TitleModel> listTitleModel =  titleModel.ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                ViewBag.CurrentFilter = searchString;
                titleModel =  listTitleModel.OfType<TitleModel>().Where (titleName => titleName.TitleName.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "TitleName":
                    titleModel = titleModel.OrderByDescending(s => s.TitleName);
                    break;
                case "ReleaseYear":
                    titleModel = titleModel.OrderByDescending(s => s.ReleaseYear);
                    break;
                default:
                    titleModel = titleModel.OrderBy(s => s.TitleName);
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(titleModel.ToPagedList(pageNumber, pageSize));
            //return View(titleModel);
        }
        
        //
        // GET: /TitleClient/Details/5

        public ActionResult Details(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TitleModel titleDetail = GetTitle(name);

            return View(titleDetail);
        }

        public static TitleModel GetTitle(string name)
        {
            return titleModel.ToList().Find(m => m.TitleName== name);
        }

    }
}
