﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TitleService.Models;

namespace TitleService.Controllers
{
    public class TitleController : ApiController
    {
        private TitlesEntities db = new TitlesEntities();

        // GET api/Title
        public async Task<IEnumerable<Title>> GetTitlesAsync()
        {
            return await Task.Run(() => db.Titles.AsEnumerable());
        }

        // GET api/Title/5
        public async Task<Title> GetTitleAsync(int id)
        {
            Title title = null;
            using (title = await Task.Run(() => db.Titles.Find(id)))
            {
                if (title == null)
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }
            }

            return title;
        }

        // GET api/Title/Casablanca
        public async Task<Title> GetTitleAsync(string name)
        {
            Title title = null;
            using (title = await Task.Run(() => db.Titles.Find(name)))
            {
                if (title == null)
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }
            }

            return title;
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}