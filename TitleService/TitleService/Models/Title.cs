using System;
using System.Collections.Generic;

namespace TitleService.Models
{
    public partial class Title : IDisposable
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string TitleNameSortable { get; set; }
        public int?TitleTypeId { get; set; }
        public int?ReleaseYear { get; set; }
        public DateTime?ProcessedDateTimeUTC { get; set; }

        void IDisposable.Dispose()
        {
        }
    }
}
